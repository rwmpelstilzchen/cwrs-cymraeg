#!/usr/bin/python3

import argparse
import dominate
from dominate import tags
import icu
import os
import re
import yaml
import pdb

PAGE_TITLE = "מקראה בוולשית עם גלוסר עברי"
PAGE_INTRO = """
<p>עדכון אחרון: 2020/04/28</p>

<p>המקראה הזאת נועדה לסייע לתלמידות/ים ב<a href=\"/ling/cymraeg/cwrs/\">קורס הוולשית</a> שאני מלמד באוניברסיטה העברית (<a href=\"http://shnaton.huji.ac.il/index.php/NewSyl/41581/1/2020/\">#41581</a>). אם יהיו אחרות/ים שימצאו בו תועלת, מה טוב.</p>

<p>כמה נקודות בסדר אקראי:</p>

<ul>
        <li>לחיצה על כל מילה מובילה לערך המתאים בגלוסר (רשימת המילים למטה), מלבד בשלושת הטקסטים האחרונים, שמהווים קריאת רשות.</li>
        <li>הגלוסר תפור בדיוק לטקסטים המופיעים במקראה, לא פחות ולא יותר (כלומר, לא מופיעות בו מילים שנעדרות מהטקסטים, וכל המילים שבטקסטים מופיעות).</li>
        <li>כל צורות הנטיה בטקסט מנותחות מורפולוגית בקצרה.</li>
        <li>לא לכל הצורות שנמנות תחת bod בגלוסר התנהגות תחבירית־מבנית זהה. הבחירה לקבץ אותן ביחד היא פרקטית; בהבדל ביניהן נדון בשיעורים.</li>
        <li>אין התייחסות למוטציות בגלוסר.</li>
        <li>כמו בכל שפה, מילות־יחס אי אפשר באמת לתרגם באופן מילוני. מה שמופיע כאן בגלוסר הוא רק קירוב כללי.</li>
        <li>ריחוף של הסמן מעל קיצור או ראשי־תיבות (כמו „ש״ת” בשביל „שם תואר”) יציג את הפירוש.</li>
        <li>העדפתי לקצר בהגדרות; בשביל פירוט יש את ה־<a href=\"http://www.geiriadur.ac.uk/\">GPC</a>.</li>
        <li>הערכים בגלוסר מסודרים לפי <a href="https://en.wikipedia.org/wiki/Alphabetical_order#Language-specific_conventions">הסדר האלפביתי הוולשי</a>.</li>
        <li>החלוקה לחלקי דיבר לא מייצגת את המציאות הלשונית, שהיא מורכבת יותר, אלא אמורה רק להוות סוג של עזר מסורתי שנותן כיוון כללי לתפקיד ולתכונות התחביריות. נדייק ונדון בזה בשיעורים.</li>
        <li>לא הבחנתי בין סוגי הכינויים השונים בגלוסר כדי לא העמיס. נדבר על זה בשיעורים.</li>
        <li>באופן כללי העדפתי לחלק ליחידות קטנות (במקרים מסויימים גם מתחת לרמת המילה האורתוגרפית) כך שהקוראות/ים יצטרכו להרכיב אותן ביחד. זאת גישה עדיפה, לדעתי, על הצגה של היחידות באופן שמחבר אותן ביחד מראש.</li>
        <li>עבור הטקסט האחרון, Gofid, קיימת הקלטה (<a href="https://sainwales.com/store/sain/sain-scd-2471">רכישה</a>; יוטיוב: <a href="https://youtu.be/mAc3PfPjqbQ">א׳</a>, <a href="https://youtu.be/mAc3PfPjqbQ">ב׳</a>). לחיצה על הסימן ▸ תשמיע את ההקלטה של המשפט.</li>
        <li>הסיבה ששמות תואר בנקבה ממויינים תחת הרשומה בזכר נובעת מסיבות מבניות בלבד.</li>
</ul>

<p>קבצי המקור (קוד + טקסט) זמינים <a href=\"https://gitlab.com/rwmpelstilzchen/cwrs-cymraeg/tree/master/texts\">כאן</a>. הקוד לא מופתי, אלא סקריפט קטן ומטולא בפייתון, אבל הוא עובד וזה מה שחשוב. על סידור האותיות לפי הסדר הוולשי אחראית הספריה <a href="https://github.com/ovalhub/pyicu">PyICU</a>. קבצי המילון כתובים ב־<a href="https://yaml.org/">YAML</a> ונקראים בעזרת <a href="https://pyyaml.org/">PyYaml</a>. הפקת הפלט ב־HTML נעשית בעזרת <a href="https://github.com/Knio/dominate">Dominate</a>.</p>

<p>בטלפונים מוצג רק הטקסט הוולשי; במסך של מחשב או טבלט, שהוא גדול יותר, יש מקום להציג את את הטקסט המקורי. לא עשיתי בדיקות מקיפות לראות שזה עובד במכשירים ודפדפנים שונים; אם התצוגה תקולה אצלכן/ם בבקשה תגידו לי.</p>
<p>אם מצאתם טעות או שמשהו לא מובן, צרו בבקשה קשר; כשמזינים ידנית הרבה מידע למחשב תמיד יש טעויות.</p>
<p style="text-align: left">— <a href=\"https://ac.digitalwords.net/\">יודה רונן</a></p>
"""

primary = []
secondary = []
grammar = []

collator = icu.Collator.createInstance(icu.Locale('cy_GB.UTF-8'))

def read_glossary(glossdir):
    global primary, secondary, grammar
    primary = list(yaml.full_load_all(open(os.path.join(glossdir, 'primary.yaml'))))[0]
    secondary = list(yaml.full_load_all(open(os.path.join(glossdir, 'secondary.yaml'))))[0]
    grammar = list(yaml.full_load_all(open(os.path.join(glossdir, 'grammar.yaml'))))[0]

def html_head():
    tags.meta(charset='utf-8')
    #tags.link(rel="stylesheet", type="text/css", href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css")
    #tags.link(rel="stylesheet", type="text/css", href="https://cdn.jsdelivr.net/flexboxgrid/6.2.0/flexboxgrid.min.css")
    tags.script(type="text/javascript", src="https://code.jquery.com/jquery-latest.js")
    tags.link(rel="stylesheet", type="text/css", href="static/css/style.css")
    playscript = """
function onlyPlayOneIn(container) {
        container.addEventListener("play", function(event) {
                audio_elements = container.getElementsByTagName("audio")
                for(i=0; i < audio_elements.length; i++) {
                        audio_element = audio_elements[i];
                        if (audio_element !== event.target) {
                                audio_element.pause();
                                audio_element.currentTime = 0;
                        }
                }
        }, true);
}
document.addEventListener("DOMContentLoaded", function() {
        onlyPlayOneIn(document.body);
});
    """
    tags.script(dominate.util.raw(playscript))

def output_text(metadata, texts_dir):
    def primary_name(match):
        lemma = next((l for l in primary if l['label'] == match.group(1)), None)
        if lemma:
            return '<a href="#' + match.group(1) + '">' + match.group(1) + '</a>'
        else:
            sublemma = next((l for l in secondary if l['label'] == match.group(1)), None)
            if sublemma:
                return '<a href="#' + sublemma['parent'] + '">' + match.group(1) + '</a>'
            else:
                return match.group(1) + 'DEBUG-01'
    def link_parent(match):
        sublemma = next((l for l in secondary if l['label'] == match.group(2)), None)
        if sublemma:
            return '<a href="#' + sublemma['parent'] + '">' + match.group(1) + '</a>'
        else:
            return '<a href="#' + match.group(2) + '">' + match.group(1) + '</a>'
    def prepare_left(title, audio, raw):
        def indicate_sentence(blabla):
            nonlocal sentence_counter
            sentence_counter += 1
            if audio:
                return f"<span class='encircle'>{sentence_counter}</span> <a onclick='this.firstChild.play()' class='play_button'><audio src='static/audio/{title}-{str(sentence_counter).zfill(3)}.opus'></audio>▸</a>"
        #return match.group(1) + f'<a onclick="this.firstChild.play()" class="play_button"><audio src="static/audio/{stripped_text}.mp3"></audio>▸</a> ' + match.group(2)
            else: 
                return f"<span class='encircle'>{sentence_counter}</span>"
        output = []
        sentence_counter = 0
        for paragraph in raw:
            if len(paragraph) > 1:
                paragraph = re.sub(r'\*', indicate_sentence, paragraph)
                paragraph = re.sub(r'\{([^}]*)\}', primary_name, paragraph)
                paragraph = re.sub(r'\[([^]]*)\]\[([^]]*)\]', link_parent, paragraph)
                output.append(paragraph)
        return output
    def prepare_right(raw):
        def indicate_sentence(blabla):
            nonlocal sentence_counter
            sentence_counter += 1
            return "<span class='encircle'>" + str(sentence_counter) + "</span>"
        output = []
        sentence_counter = 0
        for paragraph in raw:
            if len(paragraph) > 1:
                paragraph = re.sub(r'\*', indicate_sentence, paragraph)
                output.append(paragraph)
        return output
    left = prepare_left(
            metadata['left']['title'],
            'audio' in metadata['left'] and metadata['left']['audio'] == True,
            open(os.path.join(texts_dir, metadata['left']['file']))
            )
    right = prepare_right(open(os.path.join(texts_dir, metadata['right']['file'])))
    with tags.table(cls='bilingual_text') as t:
        with tags.tr():
            tags.th(metadata['left']['title'], cls=metadata['left']['class'])
            tags.th(metadata['right']['title'], cls=metadata['right']['class'] + " rightside")
        for paragraph_no in range(len(left)):
            with tags.tr():
                if left[paragraph_no][0] == "@":
                    tags.td(tags.img(src=os.path.join("static", "img", left[paragraph_no][1:-1]), cls="common_image"), colspan="2", style="text-align: center")
                else:
                    tags.td(dominate.util.raw(left[paragraph_no]), cls=' '.join(['leftside', metadata['left']['class']]))
                    if paragraph_no < len(right):
                        tags.td(dominate.util.raw(right[paragraph_no]), cls=' '.join(['rightside', metadata['right']['class']]))

def output_glossary():
    global primary, secondary, grammar, collator
    for lemma in primary:
        if not 'name' in lemma.keys():
            lemma['name'] = lemma['label']
    primary.sort(key=lambda k: collator.getSortKey(k['name']))
    for lemma in primary:
        with tags.div(id=lemma['label']):
            tags.span(lemma['name'], cls='headword')
            if 'stem' in lemma.keys() and lemma['stem']:
                tags.span("[" + lemma['stem'] + "]", cls='stem')
            if 'ipa' in lemma.keys() and lemma['ipa']:
                tags.span("/" + lemma['ipa'] + "/", cls='ipa')
            # if 'audio' in lemma.keys():
            #     if lemma['audio']:
            #         dominate.util.raw('\n<a onclick="this.firstChild.play()" class="play_button"><audio preload="none" src="http://audio.oxforddictionaries.com/en/mp3/' + lemma['audio'] + '.mp3"></audio>▸</a>\n')
            # else:
            #     dominate.util.raw('\n<a onclick="this.firstChild.play()" class="play_button"><audio preload="none" src="http://audio.oxforddictionaries.com/en/mp3/' + lemma['name'] + '_gb_1.mp3"></audio>▸</a>\n')
            if lemma['pos']:
                pos = next((g for g in grammar if g['label'] == lemma['pos']), None)
                if pos:
                    if pos['shorthand']:
                        tags.abbr(pos['shorthand'] + '‎', title=pos['gloss'], cls='grammar')
                else:
                    tags.span('DEBUG-02')
            if lemma['gloss']:
                tags.span(lemma['gloss'], lang='hebrew', cls='gloss')
            if 'note' in lemma.keys() and lemma['note']:
                tags.span('(' + lemma['note'] + ')', lang='hebrew', cls='note')
            sublemmas = [sublemma for sublemma in secondary if sublemma['parent'] == lemma['label']]
            if sublemmas:
                for sublemma in sublemmas:
                    if not 'name' in sublemma.keys():
                        sublemma['name'] = sublemma['label']
                with tags.div(cls='sublemmas'):
                    # for sublemma in sorted(sublemmas, key=lambda k: k['name'].lower()):
                    sublemmas.sort(key=lambda k: collator.getSortKey(k['name']))
                    for sublemma in sublemmas:
                        with tags.div():
                            tags.span(sublemma['name'], cls='subheadword', id=sublemma['label'])
                            if 'ipa' in sublemma.keys() and sublemma['ipa']:
                                tags.span("/" + sublemma['ipa'] + "/", cls='ipa')
                            if 'audio' in sublemma.keys() and sublemma['audio']:
                                dominate.util.raw('\n<a onclick="this.firstChild.play()" class="play_button"><audio preload="none" src="http://audio.oxforddictionaries.com/en/mp3/' + sublemma['audio'] + '.mp3"></audio>▸</a>\n')
                            if 'form' in sublemma.keys():
                                for f in sublemma['form']:
                                    form = next((g for g in grammar if g['label'] == f), None)
                                    if form:
                                        if form['gloss']:
                                            tags.span(form['gloss'], cls='grammar')
                                        else:
                                            tags.span(form['shorthand'], cls='grammar')
                                    else:
                                        tags.span('DEBUG-03')
                            if 'gloss' in sublemma.keys() and sublemma['gloss']:
                                tags.span(sublemma['gloss'], lang='hebrew', cls='gloss')
                            if 'note' in sublemma.keys() and sublemma['note']:
                                tags.span('(' + sublemma['note'] + ')', lang='hebrew', cls='note')


def output_intro():
    with tags.div(style="direction: rtl; margin-right: 3em; margin-left: auto; width: 75%"):
        tags.h1(PAGE_TITLE)
        tags.span(PAGE_INTRO)

def main():
    parser = argparse.ArgumentParser(
        description="⚙")
    parser.add_argument("-t", "--texts", type=str,
                        help="texts directory")
    parser.add_argument("-g", "--glossary", type=str,
                        help="glossary directory")
    args = parser.parse_args()

    read_glossary(args.glossary)

    doc = dominate.document(title=PAGE_TITLE)
    with doc.head:
        html_head()
    with doc.body:
        with tags.div(style="height: 60vh; overflow-y: scroll"):
            #output_text(open(args.left), open(args.right))
            output_intro()
            texts = list(yaml.full_load_all(open(os.path.join(args.texts, 'texts.yaml', ))))[0]
            for text in texts:
                output_text(text, args.texts)
        with tags.div(style="height: 35vh; overflow-y: scroll; margin-top: 1em"):
            output_glossary()
    print(dominate.util.unescape(str(doc)))


if __name__ == "__main__":
    main()
